# Li-vue

> 一款让你感到舒适又美观的vue，采用vue-cli4脚手架，Element UI 搭建的一款适合企业级管理的系统。

## 登录页面

![登录页面](https://images.gitee.com/uploads/images/2019/1227/000915_acd1bf5c_2003040.png "屏幕截图.png")

## 系统页面
![系统页面](https://images.gitee.com/uploads/images/2020/0227/193215_547de3b9_2003040.png "屏幕截图.png")

## 账户密码
- 账户：admin  
- 密码：123456
## Build Setup
``` bash
# 第一步 安装依赖
npm install
# 运行 项目 访问http://localhost:8080 
npm run dev
# build for production with minification
npm run build
# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
